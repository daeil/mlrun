## MLRun (Currently not ready for use)
The basic idea behind this is to create a web app that seamlessly integrates [BNPy](https://bitbucket.org/michaelchughes/bnpy/) with a simple graphical web interface that will allow for seamless use of some of the most state of the art Bayesian 
nonparametric models. Visualizations will be written in [d3.js](http://d3js.org/)
and the backend using a combination of redis/gunicorn/sqlite. 
