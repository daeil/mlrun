#!/usr/bin/env python
# encoding: utf-8
import os
import unittest

from config import basedir
from mlrun import app, db, admin
from mlrun.models import User, Experiment, Report, Data
from mlrun import admin
import shutil


def create_db_entries():
	app.config['TESTING'] = True
	app.config['CSRF_ENABLED'] = False
	app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + os.path.join(basedir, 'tmp/app.db')
	
	# remove all folders within the users directory
	remove_dir(app.config['USER_DIRECTORY']) 
	
	# recreate database structure
	db.drop_all()
	db.create_all()
	
	# username and passwords for mock db fill
	usernames = ['daeil','anna','jesus']
	passwords = ['bulgogi','kalbi','gochu']

	# Create a bunch of users
	for i in xrange(len(usernames)):
		add_user(usernames[i],passwords[i])
		
	
	# Create a bunch of datasets

	# Create a bunch of experiments

	# create a bunch of reports
	
	check_db()

def check_db():
	query_users = User.query.all()
	query_reports = Report.query.all()
	query_exp = Experiment.query.all()
	query_data = Data.query.all()
	
	print query_users
	print query_reports
	print query_exp
	print query_data

def remove_dir(folder):
	for the_file in os.listdir(folder):
		file_path = os.path.join(folder, the_file)
		if os.path.isdir(file_path):
			print "Deleting: " + file_path
			shutil.rmtree(file_path)

def add_user(username, password):
	''' When we add a new user, we first check if this user exists. If not,
	we create this users directory structure.'''
	admin.create_user_dir(username)
	email = username + "@mlrun.com"
	u = User(username = username, password=password, email = email)
	db.session.add(u)
	db.session.commit()

if __name__ == '__main__':
	create_db_entries()