# models.py contains code for defining the user object and behavior which will be used throughout the site
from mlrun import db, app
import datetime

# Defines a User class that takes the database and returns a User object that contains id,nickname,email
class User(db.Model):
	id = db.Column(db.Integer, primary_key = True)
	username = db.Column(db.String(64), index = True, unique = True)
	password = db.Column(db.String(64), index = True)
	email = db.Column(db.String(120), index = True, unique = True)
	reports = db.relationship('Report', backref = 'author', lazy = 'dynamic')
	datasets = db.relationship('Data', backref = 'author', lazy = 'dynamic')
	experiments = db.relationship('Experiment', backref = 'author', lazy = 'dynamic')
	
	def __init__(self, username, password, email):
		self.username = username
		self.password = password
		self.email = email

	def is_authenticated(self):
		return True

	def is_active(self):
		return True

	def is_anonymous(self):
		return False

	def get_id(self):
		return unicode(self.id)

	def __repr__(self):
		return '<User %r>' % (self.username)

	def check_password(self, proposed_password):
		if self.password != proposed_password:
			return False
		else:
			return True
						
class Report(db.Model):
	''' This represents the Reports table which contains all the reports
	created on MLRun. Each report is associated with the owner_id and corresponds 
	to the dataset in which the report is based off of. For each dataset
	there will ultimately be multiple types of analyses. 
	'''
	id = db.Column(db.Integer, primary_key = True)
	owner_id = db.Column(db.Integer, db.ForeignKey('user.id'))
	data_id = db.Column(db.Integer, db.ForeignKey('data.id'))
	name = db.Column(db.String(100), unique = True )
	content = db.Column(db.Text )
	status = db.Column(db.String(20) )
	timestamp = db.Column(db.DateTime)
	snapshots = db.Column(db.String(100))
	
	def __init__(self, name, data_id, content=None):
		self.name = name
		self.timestamp = datetime.datetime.utcnow()
		self.data_id = data_id
		if content is not None:
			self.content = content
		else:
			self.content = "Currently no content exists for this report." 

	def __repr__(self):
		return '<Report %r>' % (self.name)

class Experiment(db.Model):
	''' Each dataset can have multiple analyses. This comes into play when a user
	is attempting to create a new report. They choose the dataset and the analysis
	that was done on that dataset. They can then pull the appropriate visualizations
	associated with that dataset. 
	'''
	id = db.Column(db.Integer, primary_key = True)
	owner_id = db.Column(db.Integer, db.ForeignKey('user.id')) # the person who created this
	data_id = db.Column(db.Integer, db.ForeignKey('data.id')) # the dataset that was used
	name = db.Column(db.String(100), unique = True) # this should be unique to diffentiate it
	model = db.Column(db.String(100)) # name of the model i.e topic_model
	status = db.Column(db.Integer ) # status of the dataset (percent complete)
	likes = db.Column(db.Integer) # the number of likes for that particular analysis
	
	def __init__(self, name, data_dir):
		self.name = name
		self.timestamp = datetime.datetime.utcnow()
		self.owner_id = owner_id
		self.data_id = data_id
		self.model = model
		self.likes = 0	
		
	def __repr__(self):
		return '<Experiment %r>' % (self.name)

class Data(db.Model):
	''' This represents the Data table which contains all the datasets for MLRun. 
	Each dataset is associated with the owner_id who originally uploaded it. 
	There should be a summary, 
	'''
	id = db.Column(db.Integer, primary_key = True)
	owner_id = db.Column(db.Integer, db.ForeignKey('user.id'))
	name = db.Column(db.String(100), unique = True ) #name of the dataset inputed by the user
	savename = db.Column(db.String(100), unique = True) #name of the filename
	summary = db.Column(db.Text) # summary of the dataset (optional)
	status = db.Column(db.Text) # summary of the dataset (optional)
	N = db.Column(db.Integer) # number of datapoints
	data_dim = db.Column(db.Integer)
	data_type = db.Column(db.String(10)) # "text", "graph", "others?"
	timestamp = db.Column(db.DateTime)
	experiments = db.relationship('Experiment', backref = 'dataset', lazy = 'dynamic')
	
	def __init__(self, name, savename, owner_id, summary=None):
		self.name = name 
		self.savename = savename # this might be different from the dataset name
		self.owner_id = owner_id
		self.timestamp = datetime.datetime.utcnow()
		if summary is not None:
			self.summary = summary
		else:
			self.summary = "No summary provided yet."
	
	def __repr__(self):
		return '<Data %r>' % (self.name)