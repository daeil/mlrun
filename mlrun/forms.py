# Define a class LoginForm that controls the form object which returns true if validated correctly
from flask_wtf import Form
from wtforms import TextField, TextAreaField, PasswordField, BooleanField
from wtforms.validators import DataRequired
from models import User

class LoginForm(Form):
	username = TextField('username', validators=[DataRequired()])
	password = PasswordField('Password', validators=[DataRequired()])
	remember_me = BooleanField('remember_me', default = False)
	def __init__(self, *args, **kwargs):
		Form.__init__(self, *args, **kwargs)
		self.user = None
		
	def validate(self):
		rv = Form.validate(self)
		if not rv:
			print "not successful"
			return False
		
		user = User.query.filter_by( username=self.username.data ).first()
		if user is None:
			self.username.errors.append("Unknown username")
			print "Unknown username"
			return False
			
		if not user.check_password(self.password.data):
			self.password.errors.append('Invalid password')
			print "Wrong Password"
			return False
			
		self.user = user
		return True
		
class UploadForm(Form):
	upload_title = TextField('upload_title', validators=[DataRequired()], default='Insert Data Title Here')
	upload_summary = TextAreaField('upload_summary', validators=[DataRequired()], default='Insert Data Summary Here')
	
class ReportForm(Form):
	report_title = TextField('report_title', validators=[DataRequired()], default="Insert Report Title Here")
