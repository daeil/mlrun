# -*- coding: utf-8 -*-
from __future__ import with_statement
from flask import Flask, request, session, g, redirect, url_for, render_template
from flask.ext.sqlalchemy import SQLAlchemy
from flask.ext.login import LoginManager
from werkzeug import secure_filename
#from config import basedir
import os

#__init__.py allows you to say from <directory_containing_init.py> import app

# create our little application :)
app = Flask(__name__)
app.config.from_object('config')

# SQLAlchemy is an ORM wrapper where db is now an object that handles all connections to the database
# db can be used to connect, add, delete, etc using object methods
#app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://daeil:dcntML1982@localhost/mlrun'
db = SQLAlchemy(app)

# LoginManager simplifies the tracking 
lm = LoginManager()
lm.init_app(app)
lm.login_view = 'login'

# Reimport views and models to allow for easy reference
from mlrun import views, models
