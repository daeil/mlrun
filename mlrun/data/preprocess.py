import os
import fnmatch
import re
from operator import itemgetter
import sys
import getopt
import string
import glob
from collections import defaultdict

#celery = Celery('tasks', broker=app.config['CELERY_BROKER_URL'])
#celery = Celery('mlrun.tasks', broker='redis://localhost:6379', backend='redis://localhost:6379')

#@celery.task()

STOPWORDFILEPATH = 'mlrun/static/old/topicmodel/english_stopwords.txt'

############################################################################  MAIN SCRIPT
# n is the number of documents you wish to threshold
# thr is the number of minimum documents in which a term must appear
# p is the maximum percentage of documents a word can appear in
def tokenize_docs(DATADIR, SAVEDIR, n, thr, p, red):
  # use dictionary to summarize corpus
  #   keys are document numbers, values are full text
  #   so C[d] yields the full text of document d
  fPaths = buildCorpusFilepathList(DATADIR, n)
  # ========================================   COMPUTE RAW OCCURANCE COUNTS FOR CORPUS
  # V is a set to represent the vocabulary
  # -------------------------------------------------------------    
  # WC keeps word counts for each unique vocab term in the corpus
  #   e.g. if word 'seagull' appears ten times in corpus,
  #            then WC['seagull'] = 10
  #-----------------------------------------------------------------
  # DC keeps count of how many documents each vocab term appears in
  #    if word 'hadrosaur' appears in two different documents,
  #            then DC['hadrosaur'] = 2
  #print fPaths
  (V,WC,DC) = buildCorpusBoWdict( fPaths )
  red.publish('upload', "%s" % ( str(len(V)) + ' unique raw vocab terms discovered.\n'))
  red.publish('upload', "%s" % ('Now filtering out terms that are too frequent or too common...\n'))

  # ========================================   FILTER VOCAB BY DOCUMENT COUNT
  #  keepers is a list of vocab terms satisfying the tf-idf conditions
  #    occuring in at most MAX_DOC_PERCENTAGE of total documents
  #     and also at least in MIN_DOC_THRESHOLD documents
  keepers = get_final_vocab_list( WC , DC, n, p, thr)
  red.publish('upload', "%s" % ( str(len(keepers)) + ' unique terms extracted as final vocabulary.\n'))
  red.publish('upload', "%s" % ('Each appears in at least '+ str(thr) + ' unique documents, '))
  red.publish('upload', "%s" % ('but not more than ' + str(100*p) + '% of documents. \n'))
  keepDict = dict( (keepers[k], k+1) for k in range(len(keepers)) )
    
  # =========   BUILD TERMRANK -> TERMSTR DICT   ===============
  #  TermRank['sarcophagus'] gives rank of 'sarcophagus' in the corpus
  #   rank of 1 means most popular,  larger rank means less popular
  terms = zip( range(1, len(keepers)+1 ), keepers )
  TermRank = dict( [(termstr, rank) for rank, termstr in terms ] )

  # build TermCount dictionary using only popularWords
  #   TermCount[345] gives wordcount of term #345 in corpus
  TC = dict( [(TermRank[k],v) for k,v in WC.items() if (k in keepDict)] )

  # =========   BUILD WORDCOUNT by DOCUMENT DICT   ==================
  # DWC : word counts for each final vocab term within each document
  #    if word 'seagull' appears twice in document d,
  #       then DWC[d]['seagull'] = 2
  DWC = buildCorpusDWCdict( fPaths, keepDict )

  # =======   WRITE RESULTS to FILE   ========
  #print DWC
  output_wordcounts( SAVEDIR, DWC )
  output_vocab( SAVEDIR, terms, TC )
  output_corpus( SAVEDIR, fPaths)


############################################################################  MISCELLANEOUS FUNCTIONS
def extract_word_list( text ):
    ''' Returns list of words found in String. Matches A-Za-z and 's '''
    wordPattern = "[A-Za-z]+[']*[A-Za-z]*"  
    wordlist = re.findall( wordPattern, text)
    return wordlist

def build_stopword_set( fPath=STOPWORDFILEPATH ):
    ''' Returns list of stopwords from specified file.  '''    
    SW = set()
    stopwordfile = open( fPath )
    for line in stopwordfile.readlines():
    	word = line[:-1] # remove newline
    	SW.add( word )
    return SW

def get_final_vocab_list( WC , DC, d, p, thr):
    keepers = list()
    # sortedWC : word counts corpus wide, sorted in descending order
    #   it is a list, each entry is a (termstr, wordcount) tuple
    sortedWC = sorted(WC.items(), key=itemgetter(1), reverse=True)   
    for termstr,wordcount in sortedWC:
      doccount = DC[termstr]
      if doccount > d * p:
      	continue
      if doccount >= thr:
      	keepers.append(termstr)
    return keepers

'''
Obtain list whose i-th entry is the full path to the i-th file in the corpus
'''
def buildCorpusFilepathList(DATADIR, nMax):
  nCur = 0
  #fList = os.listdir( DATADIR )
  search = DATADIR + "/*.txt"
  fList = glob.glob(search)
  if len(fList) > nMax:
      goodPaths = fList
  else: 
	    goodPaths = fList[1:nMax]
  return goodPaths

def buildCorpusDWCdict( fPaths, kDict ):
  DWC = defaultdict( lambda: defaultdict(int) )
  for dc in range( len(fPaths) ):
    wText = extractTextFromField( fPaths[dc] )
    wlist = extract_word_list( wText.lower() )
    for w in wlist:
      if w in kDict:
        DWC[dc+1][ kDict[w]  ] += 1
  return DWC

def buildCorpusBoWdict( fPaths ):
   V = set()
   WC = defaultdict(int)
   DC = defaultdict(int)
   stopwords = build_stopword_set()
   for dc in range( len(fPaths) ):
     wText = extractTextFromField( fPaths[dc] )
     wlist = extract_word_list( wText.lower() )
     wset = set(wlist)
     wset = wset.difference( stopwords )
     # only add words to vocab if not stopwords
     V = V.union( wset  )
     for w in wlist:
       if w in stopwords:
         continue 
       WC[w] += 1   	
     for w in wset:
       DC[w] += 1
   return (V, WC, DC)    

def extractTextFromField( fPath ):
  rawtext = list()
  fid = open( fPath, 'rt')
  doCollect = 0
  for fLine in fid.readlines():
    rawtext.append( fLine.strip() + ' ' )
  return string.join(rawtext, '')

######################################################################### WRITE TO FILE FUNCTIONALITY
def output_wordcounts(DATADIR, DWC ):
  '''  Print the DWC to file wordcounts.csv'''
  outfile = open(DATADIR + '/wordcounts.csv', 'w')
  for docnum, docTC in DWC.items():
    for termnum, count in docTC.items():
      lineStr = str(docnum) + ','
      lineStr += str(termnum) + ','
      lineStr += str(count) + '\n'
      outfile.write( lineStr)
  outfile.close()
  print 'Wrote document wordcount CSV data to file: ', outfile.name

def output_vocab( DATADIR, terms, TC ):
  '''  Print TermStrs and TermNums to file vocab.txt '''
  outfile = open(DATADIR + '/vocab.txt', 'w')
  #outfile.write('###  USAGE ---   5: diplodocus (545) means the term "diplodocus" appeared 545 times in the corpus, making it the 5th most popular term \n')
  for rank, termstr in terms:
    outfile.write( termstr + '\n')
    #outfile.write( str(rank) + ': ' + termstr + ' (' + str(TC[rank]) + ')' + '\n')
  outfile.close()
  print 'Wrote vocab to file: ', outfile.name

def output_corpus( DATADIR, fPaths):
  '''  Print file paths to each doc in corups to file corpus.txt '''
  outfile = open(DATADIR + '/corpus.txt', 'w')
  for f in fPaths:
    outfile.write( f + '\n')
  outfile.close()
  print 'Wrote corpus filepaths to file: ', outfile.name