Asterisk Toy Data. Ktrue=8. D=2.
  num obs: 25000
Allocation Model:  Finite mixture with K=3. Dir prior param 1.00
Obs. Data  Model:  Gaussian distribution
Obs. Data  Prior:  None
Learn Alg: EM
Trial  1/1 | alg. seed: 4226944 | data order seed: 8541952
savepath: /Users/daeil/Dropbox/web/mlrun/mlrun/static/users/daeil/experiments/stories_test/AsteriskK8/MixModel/Gauss/EM/defaultjob/1
        0/300 after      0 sec. | K    3 | ev -9.543686320e+11 
        1/300 after      0 sec. | K    3 | ev -5.002569350e+04 
        2/300 after      0 sec. | K    3 | ev -4.913238141e+04 
        5/300 after      0 sec. | K    3 | ev -4.497143847e+04 
       10/300 after      0 sec. | K    3 | ev -3.974404471e+04 
       15/300 after      0 sec. | K    3 | ev -3.974404445e+04 
       20/300 after      1 sec. | K    3 | ev -3.974404445e+04 
       25/300 after      1 sec. | K    3 | ev -3.974404445e+04 
       30/300 after      1 sec. | K    3 | ev -3.974404445e+04 
       35/300 after      1 sec. | K    3 | ev -3.974404445e+04 
       40/300 after      1 sec. | K    3 | ev -3.974404445e+04 
       45/300 after      1 sec. | K    3 | ev -3.974404445e+04 
       50/300 after      1 sec. | K    3 | ev -3.974404445e+04 
       55/300 after      1 sec. | K    3 | ev -3.974404445e+04 
       60/300 after      2 sec. | K    3 | ev -3.974404445e+04 
       65/300 after      2 sec. | K    3 | ev -3.974404445e+04 
       70/300 after      2 sec. | K    3 | ev -3.974404445e+04 
       75/300 after      2 sec. | K    3 | ev -3.974404445e+04 
       80/300 after      2 sec. | K    3 | ev -3.974404445e+04 
       85/300 after      2 sec. | K    3 | ev -3.974404445e+04 
       90/300 after      2 sec. | K    3 | ev -3.974404445e+04 
       95/300 after      2 sec. | K    3 | ev -3.974404445e+04 
      100/300 after      2 sec. | K    3 | ev -3.974404445e+04 
      105/300 after      2 sec. | K    3 | ev -3.974404445e+04 
      110/300 after      3 sec. | K    3 | ev -3.974404445e+04 
      115/300 after      3 sec. | K    3 | ev -3.974404445e+04 
      120/300 after      3 sec. | K    3 | ev -3.974404445e+04 
      125/300 after      3 sec. | K    3 | ev -3.974404445e+04 
      130/300 after      3 sec. | K    3 | ev -3.974404445e+04 
      135/300 after      3 sec. | K    3 | ev -3.974404445e+04 
      140/300 after      3 sec. | K    3 | ev -3.974404445e+04 
      145/300 after      3 sec. | K    3 | ev -3.974404445e+04 
      150/300 after      3 sec. | K    3 | ev -3.974404445e+04 
      155/300 after      4 sec. | K    3 | ev -3.974404445e+04 
      160/300 after      4 sec. | K    3 | ev -3.974404445e+04 
      165/300 after      4 sec. | K    3 | ev -3.974404445e+04 
      170/300 after      4 sec. | K    3 | ev -3.974404445e+04 
      175/300 after      4 sec. | K    3 | ev -3.974404445e+04 
      180/300 after      4 sec. | K    3 | ev -3.974404445e+04 
      185/300 after      4 sec. | K    3 | ev -3.974404445e+04 
      190/300 after      4 sec. | K    3 | ev -3.974404445e+04 
      195/300 after      4 sec. | K    3 | ev -3.974404445e+04 
      200/300 after      5 sec. | K    3 | ev -3.974404445e+04 
      205/300 after      5 sec. | K    3 | ev -3.974404445e+04 
      210/300 after      5 sec. | K    3 | ev -3.974404445e+04 
      215/300 after      5 sec. | K    3 | ev -3.974404445e+04 
      220/300 after      5 sec. | K    3 | ev -3.974404445e+04 
      225/300 after      5 sec. | K    3 | ev -3.974404445e+04 
      230/300 after      5 sec. | K    3 | ev -3.974404445e+04 
      235/300 after      5 sec. | K    3 | ev -3.974404445e+04 
      240/300 after      5 sec. | K    3 | ev -3.974404445e+04 
      245/300 after      6 sec. | K    3 | ev -3.974404445e+04 
      250/300 after      6 sec. | K    3 | ev -3.974404445e+04 
      255/300 after      6 sec. | K    3 | ev -3.974404445e+04 
      260/300 after      6 sec. | K    3 | ev -3.974404445e+04 
      265/300 after      6 sec. | K    3 | ev -3.974404445e+04 
      270/300 after      6 sec. | K    3 | ev -3.974404445e+04 
      275/300 after      6 sec. | K    3 | ev -3.974404445e+04 
      280/300 after      6 sec. | K    3 | ev -3.974404445e+04 
      285/300 after      7 sec. | K    3 | ev -3.974404445e+04 
      290/300 after      7 sec. | K    3 | ev -3.974404445e+04 
      295/300 after      7 sec. | K    3 | ev -3.974404445e+04 
      300/300 after      7 sec. | K    3 | ev -3.974404445e+04 
... done. converged.
