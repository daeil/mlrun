$("#knob_cluster").knob({
	release : function (value) {
		if (value == 0) {
			var knob1_text = 'Automatically Learning the Optimal Number of Clusters';
			$('#knob_cluster_text').text(knob1_text);
		}
		else {
			var knob1_text = 'Fixing Algorithm to Learn ' + value + ' Clusters.';
			$('#knob_cluster_text').text(knob1_text);
		}
		
    }
});

$("#knob_minibatch").knob({
	release : function (value) {
		var knob2_text = value + '% of the Data will be Analyzed at Every Iteration';
		$('#knob_minibatch_text').text(knob2_text);
    }
});

$("#knob_runs").knob({
	release : function (value) {
		if (value == 1) {
			var knob3_text = 'Initializing Algorithm with a Single Run';
			$('#knob_runs_text').text(knob3_text);
		}
		else {
			var knob3_text = 'Initializing Algorithm with ' + value + ' Runs.';
			$('#knob_runs_text').text(knob3_text);
		}
		
    }
});