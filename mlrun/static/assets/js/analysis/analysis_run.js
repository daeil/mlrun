function analysis_run(elbo_val) { 
	var url=$("#data_run").attr('action');
	xhr(url);
	init();
}

function xhr(url) {
	var xhr = new XMLHttpRequest();
	xhr.open("GET", url, true);
	xhr.send();
}

function init() {
	var evtSrc = new EventSource("/stream_analysis");
	evtSrc.onmessage = function(e) {
		if (e.data=='exit') {
			evtSrc.close();
		}
		else {
			if (e.data!='1'){
				var tmp = e.data.split(":");
				msgType = tmp[0];
				if ((msgType == 'iter') && (parseInt(tmp[1]) != 0 )){
					var msgIter = parseInt(tmp[1]);
					var float_elbo = parseFloat(tmp[2]); // splits (iter:(iterid):elbo_value)
					elbo_val.data[0].push(float_elbo);
					elbo_val.iter.push(msgIter)
					if (tmp[1] > 9) {
						container.datum(elbo_val).transition().ease("interpolate").call(elbo);	
					}			
				}
			}
		}
	};
}

