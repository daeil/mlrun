
// Initialize gridster   
var gridster;
var delete_id;
var id_count = 3;

$(function(){
	// Allow for autosizing textareas
    gridster = $(".gridster ul").gridster({
      widget_base_dimensions: [86, 60],
      widget_margins: [2, 2],
      helper: 'clone',
      resize: {
        enabled: true
      }
    }).data('gridster');

	// Initialize a dialog box if we delete a widget
	$( "#dialog-confirm" ).dialog({
	   autoOpen: false,
	   resizable: false,
	   height:140,
	   modal: true,
	   buttons: {
	     "Delete?": function() {
			var listItem = document.getElementById( delete_id );
			var eqfield =  $( ".gridster li" ).index( listItem );
			gridster.remove_widget( $(".gridster li").eq(eqfield) );
	       $( this ).dialog( "close" );
	     },
	     Cancel: function() {
	       $( this ).dialog( "close" );
	     }
	   }
	 });
 });

// This is the d3.js interactive button: TODO
$( '#interact').click( function() {
	// swap the inner html with div element from viz
	var viz = '<div id="viz"></div>';
	$("#article_main_img").replaceWith(viz);
	var temp = $("#main_panel").html();
});

// Enable slideshow view of saved snapshots	
$( '#carousel' ).elastislide( {
	orientation : 'horizontal',
	minItems: 4
} );


// Preview Modes	
$("#preview_text").click(function() {
	$(".article_edit").toggle();
	$("#back_text").toggle();
	$('#style_report').attr('href',"/static/assets/css/report/view_report.css");
	$('#visual_nav').toggle();
});

$("#back_text").click(function() {
	$("#back_text").toggle();
	$(".article_edit").toggle();
	$('#style_report').attr('href',"/static/assets/css/report/new_report.css");
	$('#visual_nav').toggle();
});

// Clicking on this carousel adds a image to the interactive interface
$( "#carousel li img" ).click(function() {
	var snapshot = $(this).attr("src");
	var list_append = '<li class="img_insert" id="box_' +  id_count  + '""><img class="img_box_insert" src=" ' + snapshot + ' "></img></li>';
	var widget_img = [list_append, 1, 1];
	gridster.add_widget.apply(gridster, widget_img)
	id_count += 1;
});

// Clicking on the add text button should add a text-editable box
$("#add_text").click(function() {
	if ($("#move_text").text() == "Edit Text") {
		var list_append = '<li class="text_insert" id="box_' +  id_count  + '"><p class="textarea_insert">Text Filler</p></li>';
	}
	else {
		var list_append = '<li class="text_insert" id="box_' +  id_count  + '"><textarea class="textarea_insert">Text Filler</textarea></li>';
	} 
	id_count += 1;	
	var widget_text = [list_append, 1, 2];
	gridster.add_widget.apply(gridster, widget_text)
	
	$( "p.textarea_insert" ).dblclick(function() {
	  delete_id = $(this).closest("li").attr("id");
	  $( "#dialog-confirm" ).dialog( "open" );
	});
});

$("#move_text").click(function() {
	// We're wanting to move text here so we should save what's in textarea_insert
	if ($(this).text() == "Move / Delete") {
		$(".textarea_insert").each(function() {
			var cur_text = $(this).val();
			var boxid = $(this).attr("id");
		    $(this).replaceWith( "<p class=textarea_insert>" + cur_text + "</p>" );
		});
		/*
		$(".textarea_insert").replaceWith(function(){
		    return $("<p class=textarea_insert />").append($(this).text());
		});*/
		$(this).text("Edit Text");
		
		$( ".textarea_insert" ).dblclick(function() {
		  delete_id = $(this).closest("li").attr("id");
		  $( "#dialog-confirm" ).dialog( "open" );
		});
		
		$( ".img_box_insert" ).dblclick(function() {
		  delete_id = $(this).closest("li").attr("id");
		  $( "#dialog-confirm" ).dialog( "open" );
		});
		
	}
	// Button says edit text, we want to bring it back to textarea...
	else {
		$(".textarea_insert").each(function() {
			var cur_text = $(this).text();
			var boxid = $(this).attr("id");
		    $(this).replaceWith( "<textarea class=textarea_insert>" + cur_text +"</textarea>" );
		});
		/*
		$(".textarea_insert").replaceWith(function(){
		    return $("<textarea class=textarea_insert />").append($(this).text());
		});*/
		$(this).text("Move / Delete");
	}
});


/*
$( "#carousel li img" ).click(function() {
	$(".article_canvas").css("border","none");
	var main_src = $("#article_main_img").attr("src");
	var list_src = $(this).attr("src");
	var list_append = ' <li><a href="#"><img class="article_canvas" src=" ' + main_src + ' "></a></li>'; 
	var main_replace = ' <img id="article_main_img" src="' + list_src + '" alt=""> ';
	
	$(this).css("border","3px solid #111");
	$('#article_main_img').replaceWith(main_replace);
});
*/