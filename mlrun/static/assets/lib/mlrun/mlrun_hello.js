// Starting a new namespace mlrun to create a modularized visual system
var dataset = [1, 2, 3, 4];
// Creating namespace
d3.edge = {};

// This outside function is used simply as a container for the private functions
// within simpleChart. We then use setter/getter methods to access these private 
// methods. 
d3.edge.table = function module() {
	var fontsize = 10,
		fontcolor = 'red';
	
	// Declare: To get events out of the module
	// we use d3.dispatch, declaring a "hover" event
	var dispatch = d3.dispatch('customHover');
	
	// Trigger: 
	
	function exports(_selection) {
		// Loops through the selected elements and _data object 
		// attached to each DOM element will be used as the text
		// of our generated <div>.
		_selection.each(function(_data) {
			d3.select(this).append('div')
			.style({'font-size': fontSize + 'px', color: fontColor}) 
			.html('HelloWorld: ' + _data)
			.on('mouseover', dispatch.customHover);
			});
	}
	exports.fontSize = function(_x) {
		if (!arguments.length) return fontSize;
		fontSize = _x;
		return this;
	};
	
	exports.fontColor = function(_x) {
		if (!arguments.length) return fontColor;
		fontColor = _x;
		return this;
	};
	
	// For the event to be accessible from the outside, it needs
	// to be bound to the module itself. We use d3.rebind for this.
    // We can rebind the custom events to the "exports" function
    // so it's available under the typical "on" method
    d3.rebind(exports, dispatch, "on");
	return exports;
};

// Setters can also be chained directly to the returned function
var table = d3.edge.table().fontSize("100").fontColor("red");
// We bind a listener function to the custom event
table.on("customHover", function(d, i) { console.log("customHover: " + d, i); });

d3.select("#figure")
    .datum(dataset)
    .call(table);
