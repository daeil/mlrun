// Bar chart Module
/////////////////////////////////
// set up the d3.custom namespace
d3.custom = {};

// define the barChart module function
d3.custom.barChart = function module() {
	// define fixed constructor settings
    var margin = {top: 20, right: 20, bottom: 40, left: 40},
        width = 900,
        height = 500,
        gap = 0,
        ease = "fade"; // transition
    var svg;

    var dispatch = d3.dispatch("customHover");
    function exports(_selection) {
        _selection.each(function(_data) {
        	
        	// common practice in defining chart height and width
        	// while accounting for margins
            var chartW = width - margin.left - margin.right,
                chartH = height - margin.top - margin.bottom;

            // variables x1, y1, xAxis, yAxis are components used to 
            // draw the x and y axis
            var x1 = d3.scale.ordinal()
                    .domain(_data.map(function(d, i) { return i; }))
                    .rangeRoundBands([0, chartW], 0.1);

            var y1 = d3.scale.linear()
                    .domain([0, d3.max(_data, function(d, i) { return d; })])
                    .range([chartH, 0]);

            var xAxis = d3.svg.axis()
                    .scale(x1)
                    .orient("bottom");

            var yAxis = d3.svg.axis()
                    .scale(y1)
                    .orient("left");
            
            // this defines the width of each bar
            var barW = chartW / _data.length;
            
            // Append the main 'svg' element if it doesn't exist for this instance of the module.
            // Also append the main g elements
            // The classed attributes defines the CSS class
            
            if (!svg) {
            	// Define svg element and give it the class='chart'
                svg = d3.select(this)
                    .append("svg")
                    .classed("chart", true);
                
                // append to svg element the container group, chart group, x,y axes
                var container = svg.append("g").classed("container-group", true);
                container.append("g").classed("chart-group", true);
                container.append("g").classed("x-axis-group axis", true);
                container.append("g").classed("y-axis-group axis", true);
            }
            
            // Transition the width and height
            svg.transition().attr({width: width, height: height});
            
            // Shifts the whole chart into the right place
            svg.select(".container-group")
                .attr({transform: "translate(" + margin.left + "," + margin.top + ")"});

            svg.select(".x-axis-group.axis")
                .transition()
                .ease(ease)
                .attr({transform: "translate(0," + (chartH) + ")"})
                .call(xAxis);

            svg.select(".y-axis-group.axis")
                .transition()
                .ease(ease)
                .call(yAxis);
            
            
            // used to layout the individual bars, have to be recalculated each time the data is updated
            var gapSize = x1.rangeBand() / 100 * gap;
            var barW = x1.rangeBand() - gapSize;
            
            // This part is now the enter,update, and exit process
            
            // begin by selecting all the bars
            var bars = svg.select(".chart-group")
                    .selectAll(".bar")
                    .data(_data);
            // if there are no bars, create them
            bars.enter().append("rect")
                .classed("bar", true)
                .attr({x: chartW,
                    width: barW,
                    y: function(d, i) { return y1(d); },
                    height: function(d, i) { return chartH - y1(d); }
                })
                .on("mouseover", dispatch.customHover);
            
            // if updates exist, apply them using a transition
            bars.transition()
                .ease(ease)
                .attr({
                    width: barW,
                    x: function(d, i) { return x1(i) + gapSize / 2; },
                    y: function(d, i) { return y1(d); },
                    height: function(d, i) { return chartH - y1(d); }
                });
            
            // if exits need to happen, apply a transition and remove the DOM
            // nodes when the transition has finished the exit
            bars.exit().transition().style({opacity: 0}).remove();
        });
    }
    
    // getters and setters standard now..
    exports.width = function(_x) {
        if (!arguments.length) return width;
        width = parseInt(_x);
        return this;
    };
    exports.height = function(_x) {
        if (!arguments.length) return height;
        height = parseInt(_x);
        return this;
    };
    exports.gap = function(_x) {
        if (!arguments.length) return gap;
        gap = _x;
        return this;
    };
    exports.ease = function(_x) {
        if (!arguments.length) return ease;
        ease = _x;
        return this;
    };
    d3.rebind(exports, dispatch, "on");
    return exports;
};

// Usage
/////////////////////////////////

var chart = d3.custom.barChart();

function update() {
    var data = randomDataset();
    d3.select("#figure")
        .datum(data)
        .call(chart);
}

function randomDataset() {
	// d3.range creates an array of numbers starting at 0
	// and ending at a random number between 0 and 50 (fast way of initializing an array)
	// we then take each entry of the array and replace it with
	// a random value between 0 and 1000
    return d3.range(~~(Math.random() * 50)).map(function(d, i) {
        return ~~(Math.random() * 1000);
    });
}

update();

//setInterval(update, 10000);