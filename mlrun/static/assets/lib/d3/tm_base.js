// Topic.js all-in-one visualization browser for topic models
// Copyright MLRUN 2013

// Two types of files needed: topwords.js & topwords_prob.csv
// topwords.js contains: global topic probabilities, list of top 50 

// width should be some thing dependent on the number of datapoints
var margin = {top: 20, right: 100, bottom: 30, left: 40},
    width = 760 - margin.left - margin.right,
    height = 420 - margin.top - margin.bottom;

var x = d3.scale.ordinal().rangeRoundBands([0, width], .1);
var y = d3.scale.linear().rangeRound([height, 0]);
var z = d3.scale.category20c();
var format = d3.time.format("%b");

//var color = d3.scale.ordinal()
//    .range(["#98abc5", "#8a89a6", "#7b6888", "#6b486b", "#a05d56", "#d0743c", "#ff8c00"]);

var xAxis = d3.svg.axis()
    .scale(x)
    .orient("bottom");

var yAxis = d3.svg.axis()
    .scale(y)
    .orient("left")
    .tickFormat(d3.format(".0%"));

// Add the color topic bar
var barh = 800; // height of the topic bar section
var topicsum = d3.sum(topic_prob);
var topicbar = d3.select('#d3bars').append("svg")
	.attr("width", 70);

	topicbar.selectAll("rect")
		.data(topic_prob)
		.enter().append("rect")
		.attr("y", function(d,i) { return barh*(d3.sum(topic_prob.slice(0,i))/topicsum) ; })
		.attr("height", function(d) { return d/topicsum*barh;})
		.attr("width", 100)
		.style("fill", function(d,i) { return z(i);})
		.style("stroke", "#eee")
		.on("mouseover", function() { 
			d3.select(this).attr("opacity",.5); })
		.on("click", function(d,i) { 
			var temp = "tm_"+(i+1)+".csv";
			doc_stack(temp,i); // update document stack
			word_stack(i); // update words
			})
		.on("mouseout", function() { 
			d3.select(this).attr("opacity", 1); });
			
// Load the document proportions and document titles
var K = topic_prob.length;
doc_stack("tm_1.csv", 0);
word_stack(0);

//************ FUNCTIONS *************//

// FUNCTION: MODIFIES WORD STACK
function word_stack(topicid){
	// Add the word frequency bar
	d3.select('#d3words').selectAll("li").remove();
	d3.select('#d3words').selectAll("li")
			.data(top_words[topicid])
			.enter()
			.append("li")
			.text(function(d,i){ return top_words[topicid][i] })
			.style("font-size","15px")
			.style("font-family","courier")
			.style("list-style","none")
			.style("text-align", "center");
}

var w = 760,
    h = 200,
    p = [130, 150, 30, 20],
    x = d3.scale.ordinal().rangeRoundBands([0, w - p[1] - p[3]]),
    y = d3.scale.linear().range([0, h - p[0] - p[2]]),
    z = d3.scale.ordinal().range(["lightpink", "darkgray", "lightblue"]),
    parse = d3.time.format("%m/%Y").parse,
    format = d3.time.format("%b");

// FUNCTION: MODIFIES DOC STACK
function doc_stack(input, topicid){
var k = topicid;
var basedir = "/static/data/tm_results/";
//var fileinput = basedir + input;
var fileinput = basedir + "tm_1.csv";

d3.select("#d3body").selectAll("svg").remove();
var svg = d3.select("#d3body").append("svg")
    .attr("width", width + margin.left + margin.right)
    .attr("height", height + margin.top + margin.bottom)
  .append("g")
    .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

d3.csv(fileinput, function(crimea) {

	  // Transpose the data into layers by cause.
	  var causes = d3.layout.stack()(["wounds", "other", "disease"].map(function(cause) {
	    return crimea.map(function(d) {
	      return {x: parse(d.date), y: +d[cause]};
	    });
	  }));

	  var causes = d3.layout.stack()(crimea);
	
	  // Compute the x-domain (by date) and y-domain (by top).
	  x.domain(causes[0].map(function(d) { return d.x; }));
	  y.domain([0, d3.max(causes[causes.length - 1], function(d) { return d.y0 + d.y; })]);

	  // Add a group for each cause.
	  var cause = svg.selectAll("g.cause")
	      .data(causes)
	    .enter().append("svg:g")
	      .attr("class", "cause")
	      .style("fill", function(d, i) { return z(i); })
	      .style("stroke", function(d, i) { return d3.rgb(z(i)).darker(); });

	  // Add a rect for each date.
	  var rect = cause.selectAll("rect")
	      .data(Object)
	    .enter().append("svg:rect")
	      .attr("x", function(d) { return x(d.x); })
	      .attr("y", function(d) { return -y(d.y0) - y(d.y) + 100; })
	      .attr("height", function(d) { return y(d.y); })
	      .attr("width", x.rangeBand());

	  // Add a label per date.
	  var label = svg.selectAll("text")
	      .data(x.domain())
	    .enter().append("svg:text")
	      .attr("x", function(d) { return x(d) + x.rangeBand() / 2; })
	      .attr("y", 6)
	      .attr("text-anchor", "middle")
	      .attr("dy", ".71em")
	      .text(format);

	  // Add y-axis rules.
	  var rule = svg.selectAll("g.rule")
	      .data(y.ticks(5))
	    .enter().append("svg:g")
	      .attr("class", "rule")
	      .attr("transform", function(d) { return "translate(0," + -y(d) + ")"; });

	  rule.append("svg:line")
	      .attr("x2", w - p[1] - p[3])
	      .style("stroke", function(d) { return d ? "#fff" : "#000"; })
	      .style("stroke-opacity", function(d) { return d ? .7 : null; });

	  rule.append("svg:text")
	      .attr("x", w - p[1] - p[3] + 6)
	      .attr("dy", ".35em")
	      .text(d3.format(",d"));
	});

}

