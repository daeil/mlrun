// Topic.js all-in-one visualization browser for topic models
// Copyright MLRUN 2013

// Two types of files needed: topwords.js & topwords_prob.csv
// topwords.js contains: global topic probabilities, list of top 50 

// width should be some thing dependent on the number of datapoints

//var basedir = "/static/topicmodel/data/";
//var csvdir = "/static/topicmodel/nsf/csv/";
var basedir = "/static/topicmodel/data/";
var csvdir = "/static/topicmodel/stories/csv/";

var margin = {top: 20, right: 125, bottom: 10, left: 40},
    basew = 100,
    width = basew*8 - margin.left - margin.right,
    height = 420 - margin.top - margin.bottom;

var x = d3.scale.ordinal()
    .rangeRoundBands([0, width], .1);

var y = d3.scale.linear()
    .rangeRound([height, 0]);

var color = d3.scale.category20c();

//var color = d3.scale.ordinal()
//    .range(["#98abc5", "#8a89a6", "#7b6888", "#6b486b", "#a05d56", "#d0743c", "#ff8c00"]);

var xAxis = d3.svg.axis()
    .scale(x)
    .orient("bottom");

var yAxis = d3.svg.axis()
    .scale(y)
    .orient("left")
    .tickFormat(d3.format(".0%"));

// Add the color topic bar
var barh = 770; // height of the topic bar section
var topicsum = d3.sum(topic_prob);
var topicbanner = d3.select('#d3banner');
var topicbar = d3.select('#d3bars').append("svg")
	.attr("width", 50);

topicbar.selectAll("rect")
	.data(topic_prob)
	.enter().append("rect")
	.attr("y", function(d,i) { return barh*(d3.sum(topic_prob.slice(0,i))/topicsum) ; })
	.attr("height", function(d) { return d/topicsum*barh;})
	.attr("width", 100)
	.style("fill", function(d,i) { return color(i);})
	.style("stroke", "#eee")
	.on("mouseover", function() { 
		d3.select(this).attr("opacity",.5); })
	.on("click", function(d,i) { 
		topicbanner.text("Showing Top 100 Documents for Topic " + (i + 1));
		var temp = "tm_"+(i+1)+".csv";
		doc_stack(temp,i); // update document stack
		word_stack(i); // update words
		})
	.on("mouseout", function() { 
		d3.select(this).attr("opacity", 1); });
		
// Load the document proportions and document titles
doc_stack("tm_1.csv", 0);
word_stack(0);

//************ FUNCTIONS *************//

// FUNCTION: MODIFIES WORD STACK
function word_stack(topicid){
	// Add the word frequency bar
	d3.select('#d3words').selectAll("li").remove();
	d3.select('#d3words').selectAll("li")
			.data(top_words[topicid])
			.enter()
			.append("li")
			.text(function(d,i){ return top_words[topicid][i] })
			.style("font-size","15px")
			.style("font-family","courier")
			.style("list-style","none")
			.style("text-align", "center");
}

// FUNCTION: MODIFIES DOC STACK
function doc_stack(input, topicid){
var k = topicid;
var fileinput = csvdir + input;

d3.select("#d3body").selectAll("svg").remove();
var svg = d3.select("#d3body").append("svg")
    .attr("width", width + margin.left + margin.right)
    .attr("height", height + margin.top + margin.bottom)
  .append("g")
    .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

d3.csv(fileinput, function(error, data) {
	// load the data, look at the keys in the top row and make a unique color scheme based off of this
  color.domain(d3.keys(data[0]).filter(function(key) { return key != 'doc' ; }));

	// For each datapoint we specify d.ages to represent age groups
  data.forEach(function(d) {
    var y0 = 0;
    d.ages = color.domain().map(function(name) { return {name: name, y0: y0, y1: y0 += +d[name]}; });
    d.ages.forEach(function(d) { d.y0 /= y0; d.y1 /= y0; });
  });

  //data.sort(function(a, b) { return b.ages[0].y1 - a.ages[0].y1; });

	// This I believe takes care of the tick marks
	
  x.domain(data.map(function(d) { return d.doc; }));
  svg.append("g")
      .attr("class", "y axis")
      .call(yAxis);

  var state = svg.selectAll(".state")
      .data(data)
    .enter().append("g")
      .attr("class", "state")
			.attr("transform", function(d) { return "translate(" + x(d.doc) + ",0)"; });

  state.selectAll("rect")
      .data(function(d) { return d.ages; })
    .enter().append("rect")
      .attr("width", x.rangeBand())
      .attr("y", function(d) { return y(d.y1); })
      .attr("height", function(d) { return y(d.y0) - y(d.y1); })
      .style("fill", function(d) { return color(d.name); })
			.on("mouseover", function(d, a, b) { 
				d3.select(this).attr("opacity",.5);
				word_stack(a); 
			  d3.select('#d3header').text("Title: " + top_documents[k][b] + ", Topic: " + (a+1))
					.style("color","#006600")
					.style("font-family","helvetica")
					.style("font-size","20px");
			})
			.on("click", function(d, a, b) { 
			  doc_text = d3.text(basedir + top_documents[k][b], function(error, text) {
					d3.select('#d3text').html(text)
					.style("color", "#333")
					.style("font-size","13px"); 
				} );
			})   
			.on("mouseout", function(d) { d3.select(this).attr("opacity",1); });
});
}

