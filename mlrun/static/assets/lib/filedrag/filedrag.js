/*
filedrag.js - HTML5 File Drag & Drop demonstration
Featured on SitePoint.com
Developed by Craig Buckler (@craigbuckler) of OptimalWorks.net
*/
(function() {
	//var tempImg;
	
	// getElementById
	function $id(id) {
		return document.getElementById(id);
	}

	// output information
	function Output(msg) {
		var m = $id("filedrag_data");
		m.innerHTML = msg;
	}

	function OutputIMG(msg) {
		var m = $id("filedrag_img");
		m.innerHTML = msg;
	}

	// file drag hover
	function FileDragHover(e) {
		e.stopPropagation();
		e.preventDefault();
		e.target.className = (e.type == "dragover" ? "hover" : "");
	}


	// file selection
	function FileSelectHandlerData(e) {

		// cancel event and hover styling
		FileDragHover(e);

		// fetch FileList object
		var files = e.target.files || e.dataTransfer.files;

		// process all File objects
		for (var i = 0, f; f = files[i]; i++) {
			// This will simply output the file parameters
			ParseFile(f);
			UploadFileData(f);
			
		}
	}

	// file selection
	function FileSelectHandlerImage(e) {

		// cancel event and hover styling
		FileDragHover(e);

		// fetch FileList object
		var files = e.target.files || e.dataTransfer.files;

		// process all File objects
		for (var i = 0, f; f = files[i]; i++) {
			// This will simply output the file parameters
			ParseFile(f);
			UploadFileImage(f);
			//tempImg = f;
		}
	}

	// output file information
	function ParseFile(file) {

		Output(
			"<h3> File: " + file.name + "</h3>" +
			"<h3> Type: " + file.type + "</h3>" +
			"<h3> Size: " + file.size + " bytes </h3>"
		);
		
		// display an image
		if (file.type.indexOf("image") == 0) {
			var reader = new FileReader();
			reader.onload = function(e) {
				OutputIMG(
					'<img id="img_frame" src="' + e.target.result + '" />'
				);
			}
			reader.readAsDataURL(file);
		}
	}
	
	
	function UploadFileImage(file) {
		var xhr = new XMLHttpRequest();
		// try formdata
		var form = document.getElementById('upload_img');
		var formData = new FormData(form);
		formData.append('file', file);
  
		// start upload
		xhr.open("POST", $id("upload_img").action, true);
		xhr.setRequestHeader("X_FILENAME", file.name);
		xhr.send(formData);
	}
	
	// upload JPEG files
	function UploadFileData(file) {

		var xhr = new XMLHttpRequest();

		if (xhr.upload && file.size <= $id("MAX_FILE_SIZE").value) {

			// create progress bar
			var o = $id("upload_progress");
			var progress = o.appendChild(document.createElement("p"));
			progress.appendChild(document.createTextNode("Uploading and Preprocessing: " + file.name));
			
			// progress bar
			xhr.upload.addEventListener("progress", function(e) {
				var pc = parseInt(100 - (e.loaded / e.total * 100));
				progress.style.backgroundPosition = pc + "% 0";
			}, false);

			// file received/failed
			xhr.onreadystatechange = function(e) {
				if (xhr.readyState == 4) {
					progress.className = (xhr.status == 200 ? "success" : "failure");
					//$("#refresh").show();
				}
			};
			
			// try formdata
			var form = document.getElementById('upload_data');
			var formData = new FormData(form);
			formData.append('file', file);
			//formData.append('upload_title', $("#upload_title").text());
      		//formData.append('upload_summary', $("#upload_summary").text());

			// start upload
			xhr.open("POST", $id("upload_data").action, true);
			xhr.setRequestHeader("X_FILENAME", file.name);
			xhr.setRequestHeader("X_TITLE",  $("#upload_title").val() );
			xhr.setRequestHeader("X_SUMMARY", $("#upload_summary").text() );
			//xhr.setRequestHeader("IMAGE", tempImg);
			xhr.send(formData);
			//$('#refresh').trigger('click');
			// Turn on interval loops to check if results have finished
		}
		else {
			alert('no success');
		}
		
	}


	// initialize 
	// This initialization function defines the handlers for each of our divs
	// Each divs has a particular property, for example filedrag_data has a 
	// drop event, which fires when something like a file is dropped in this region
	// We then fire the fileSelectHandler function defined in this script which
	// then engaged in the process of opening up our XHR channel and uploading the file
	function Init() {
		var fileselect_img = $id("fileselect_img"),
			fileselect_data = $id("fileselect_data"),
			filedrag_img = $id("filedrag_img"),
			filedrag_data = $id("filedrag_data"),
			submit_img = $id("submit_img"),
			submit_data = $id("submit_data");

		// file select
		fileselect_img.addEventListener("change", FileSelectHandlerData, false);
		fileselect_data.addEventListener("change", FileSelectHandlerImage, false);
		
		// is XHR2 available?
		var xhr = new XMLHttpRequest();
		if (xhr.upload) {

			// file drop
			filedrag_data.addEventListener("dragover", FileDragHover, false);
			filedrag_data.addEventListener("dragleave", FileDragHover, false);
			filedrag_img.addEventListener("dragover", FileDragHover, false);
			filedrag_img.addEventListener("dragleave", FileDragHover, false);
			
			// validate drop before this is fired
			filedrag_data.addEventListener("drop", FileSelectHandlerData, false);
			filedrag_img.addEventListener("drop", FileSelectHandlerImage, false);
			
			filedrag_data.style.display = "block";
			filedrag_img.style.display = "block";

			// remove submit button
			fileselect_img.style.display = "none";
			fileselect_data.style.display = "none";
			submit_img.style.display = "none";
			submit_data.style.display = "none";
		}

	}

	// call initialization file
	if (window.File && window.FileList && window.FileReader) {
		Init();
	}


})();