$('#enable').click(function() {
    $('#user .editable').editable('toggleDisabled');
});

$.fn.editable.defaults.mode = 'inline';

$(function(){
$('#bio').editable({
    type: 'text',
    url: '/post',
    pk: 1,
    placement: 'top',
    title: 'Enter username'
});

$('#name').editable({
    type: 'text',
    url: '/post',
    pk: 1,
    placement: 'top',
    title: 'Enter username'
});

$('#org').editable({
    type: 'text',
    url: '/post',
    pk: 1,
    placement: 'top',
    title: 'Enter username'
});

$('#email').editable({
    type: 'text',
    url: '/post',
    pk: 1,
    placement: 'top',
    title: 'Enter username'
});

$('#website').editable({
    type: 'text',
    url: '/post',
    pk: 1,
    placement: 'top',
    title: 'Enter username'
});

$('#phone').editable({
    type: 'text',
    url: '/post',
    pk: 1,
    placement: 'top',
    title: 'Enter username'
});

//ajax emulation. Type "err" to see error message
$.mockjax({
    url: '/post',
    responseTime: 400,
    response: function (settings) {
        if (settings.data.value == 'err') {
            this.status = 500;
            this.responseText = 'Validation error!';
        } else {
            this.responseText = '';
        }
    }
});
});