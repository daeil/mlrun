# Define a set of helper functions for mlrun

from mlrun import db, app
from mlrun.models import User, Report, Experiment, Data
import os
import shutil
import zipfile
import random

def get_user_dir(username):
	basedir = os.path.join(app.config['USER_DIRECTORY'],username)
	return basedir
	
def get_user_data_dir(username):
	# All datasets should be referenced by their user? Perhaps not
	# maybe we should go with a central dataset directory?
	# Data should be stored as a hashed file name
	basedir = os.path.join(app.config['USER_DIRECTORY'],username,"datasets")
	return basedir
	
def get_user_exp_dir(username, exp_name):
	basedir = os.path.join(app.config['USER_DIRECTORY'],username,"experiments")
	return basedir
	
def get_user_report_dir(username,report_name):
	basedir = os.path.join(app.config['USER_DIRECTORY'],username,"reports")
	return basedir
	
def get_user_tmp_dir(username,report_name):
	basedir = os.path.join(app.config['USER_DIRECTORY'],username,"tmp")
	return basedir


# DATABASE RELEVANT UTILITIES
def add_data_db(name, username, savename, summary=None ):
	# add to the database
	# __init__(self, name, owner_id, summary=None):
	query = Data.query.filter_by(name=name).first()
	if query is None:
		data_record = Data(name=name, owner_id=username, summary=summary, savename=savename)
		db.session.add(data_record)
		db.session.commit()
	else:
		print "Warning, dataset already exists..."
	# save relevant files into relevant user directory	

def dataset_info(username=None, data_id=None):
	result = Data.query.filter_by(id=data_id).first()
	if result is not None:
		datainfo = result
		datafid = "users/" + username + "/datasets/" + result.savename + "/upload_image.jpg"
	else:
		datainfo=[]
		datafid=[]
	return(datainfo, datafid)

def create_user_dir(username):
	newdir = app.config['USER_DIRECTORY'] +  username
	if os.path.exists(newdir):
		print "Directory already exists for " + username
	else:
		datadir = newdir + "/datasets"
		expdir = newdir + "/experiments"
		reports = newdir + "/reports"
		tmp = newdir + "/tmp"
		os.makedirs(newdir)
		os.makedirs(datadir)
		os.makedirs(expdir)
		os.makedirs(reports) 
		os.makedirs(tmp)
		
		print "Creating directory structure for: " + newdir

def delete_user_dir(username):
	deldir = app.config['USER_DIRECTORY'] +  username
	if os.path.exists(deldir):
		print "Removing entire contents of: " + deldir
		shutil.rmtree(deldir)
	else:
		print "Directory does not exist for user " + username

# Creates a new directory structure for an uploaded zip file
def create_data_dir_structure(data_dir, fid):
	first, ext = os.path.splitext(fid.filename)
	
	base_dir = os.path.join(data_dir, first)
	raw_dir = os.path.join(data_dir, first ,"raw")
	extract_dir = os.path.join(data_dir, first , "extracted")
	processed_dir = os.path.join(data_dir, first , "processed")
	
	# Make the directory structure
	if not os.path.exists(raw_dir):
		os.makedirs(raw_dir)
		os.makedirs(extract_dir)
		os.makedirs(processed_dir)
	else:
		print "Directory structure for this dataset already exists"
	return base_dir, raw_dir, extract_dir, processed_dir

# A quick way of saving some useful file to the user's temporary directory
def save_to_tmp(username, fid):
	user_dir = get_user_dir(username)
	save_file = os.path.join(user_dir,'tmp', fid.filename) 
	print save_file
	fid.save( save_file )
	print "Saving "+ save_file
	
# Grab a random image file from a fixed directory
def grab_img_file():
	random_img = random.choice(os.listdir(app.config['RANDOM_IMG_DIRECTORY']))
	source = os.path.join(app.config['RANDOM_IMG_DIRECTORY'], random_img)
	print source
	return source
	
# Saves and extracts raw uploaded zip file
def save_data(username, fid, red):
	data_dir = get_user_data_dir(username)
	
	# retrieve the relevant directories
	base_dir,raw_dir,extract_dir,processed_dir = create_data_dir_structure(data_dir, fid)
	save_file = os.path.join(raw_dir, fid.filename) 
	fid.save( save_file ) 
	
	# check if image file exists in tmp, if so, then save to base_dir
	user_dir = get_user_dir(username)
	file_list = os.listdir(os.path.join(user_dir,'tmp'))
	
	# if we have an uploaded image, then save it to our dataset directory
	if not file_list:
		image_path = os.path.join(base_dir, "upload_image.jpg")
		source = grab_img_file() 
		shutil.copyfile(source, image_path)
		print 'using temporary image'
	# otherwise use a random image from our collection
	else:
		for files in os.listdir(os.path.join(user_dir,'tmp')):
			if files.startswith("upload_image"):
				print 'yes we found an image!'
				image_path = os.path.join(base_dir, files) 
				source = os.path.join(user_dir,'tmp', files)
				shutil.copyfile(source, image_path)
				break
	
	print image_path
	# Start unzipping file and uncompress to extract_dir
	with zipfile.ZipFile(save_file) as zip_file:
		numdoc = 0
		for member in zip_file.namelist():
			filename = os.path.basename(member)
			# skip directories + TODO: do some checks to see its actually text
			if filename:
				numdoc += 1
				source = zip_file.open(member)
				target = file(os.path.join(extract_dir, filename), "wb")
				with source, target:
					shutil.copyfileobj(source, target)
	red.publish('upload', "%s" % ('Discovered and extracted ' + str(numdoc) + ' valid documents.\n'))
	red.publish('upload', "%s" % ('Tokenizing + Preprocessing Documents now... \n'))
	
	# return the directory which will contain our final processed files
	return extract_dir, processed_dir, image_path, fid.filename
	
	

