#!/usr/bin/env python
# encoding: utf-8
"""
views.py

Created by Dae Il Kim on 2013-03-25.
Copyright (c) 2013 MLRun. All rights reserved.
"""
# From mlrun is the module containing things of interest in __init__.py
import os
import time # used to test server side events only
from gevent.queue import Queue # used to test server side events
import redis
from flask import Flask, request, session, g, redirect, url_for, abort, render_template, flash, send_from_directory, jsonify, Response
from mlrun import app, db, lm
from flask.ext.login import login_user, logout_user, current_user, login_required
from forms import LoginForm, UploadForm, ReportForm
from models import User, Data, Experiment
from werkzeug import secure_filename
from data import preprocess
from util import admin
import bnpy

# START REDIS
msgServer = redis.StrictRedis()

# before any requests assign the global variable current_user to g.user for easy reference
@app.before_request
def before_request():
	g.user = current_user

# The default page
@app.route('/')
@login_required
def newsfeed():
	return render_template('home.html')
		
# The article page - lots of stuff needs to be done with this...
@app.route('/article', methods = ['GET', 'POST'])
def article():
	return render_template('reports/reports_view.html')

# The about page, needs some work
@app.route('/about')
def about():
	return render_template('mlrun.html')

# ***************  REPORTS  ********************************* REPORTS **************************
@app.route('/<username>/reports')
def manage_reports(username=None):
	data = []
	return render_template('data/data_list.html', data=data, page='reports')

@app.route('/reports/view')
@app.route('/<username>/reports/view')
def view_report(username=None):
	form = ReportForm()
	return Response(status="200")

@app.route('/<username>/reports/new')
def new_report(username=None):
	form = ReportForm()
	return render_template('reports/reports_new.html', form=form)

@app.route('/<username>/reports/submit')
def submit_report(username=None):
	form = ReportForm()
	return Response(status="200", form=form)

# ***************  RUN-ANALYSIS  ********************************* RUN-ANALYSIS **************************
# EVENT STREAM
def event_stream(channel=None):
	''' Function that calls the redis db
	Part of the Publisher / Subscriber pattern
	'''
	pubsub = msgServer.pubsub()
	pubsub.subscribe(channel)
	# handle client disconnection in the client side by calling the exit keyword
	for message in pubsub.listen():
		yield 'data: %s\n\n' % message['data']
		
@app.route("/stream_analysis")
def stream_analysis():
	mimetype = "text/event-stream"
	return Response(event_stream(channel='analysis'), mimetype=mimetype)
	
@app.route('/<username>/data/<int:data_id>/analysis')
def run_analysis(username=None, data_id=None, methods=['GET','POST']):
	result = Data.query.filter_by(id=data_id).first()
	if (request.method == 'GET') and (result is not None):
		msgServer.publish('analysis', "%s" % ('Starting bnpy analysis'))
		query = Data.query.filter_by(id=data_id).first()
		query.status = 'inprogress'
		db.session.commit()
		hmodel = bnpy.Run.run('AsteriskK8', 'MixModel', 'Gauss', 'EM', K=3, nLap=300, userName=username, fidName=result.savename, msgServer=msgServer)
		msgServer.publish('analysis', "%s" % ('Finished bnpy analysis'))
		msgServer.publish('analysis', "%s" % ('exit'))
		#admin.add_exp_db( name = dataset_name, username=g.user.username, savename=savefname, summary=summary)
		query = Data.query.filter_by(id=data_id).first()
		query.status = 'finish'
		db.session.commit()
		return Response(status='200')
	else:
		return Response(status='404')
	
# ***************  DATA  ********************************* DATA **************************

# DATASET LISTING USER SPECIFIC
@app.route('/<username>/data')
def manage_data(username=None):
	result = Data.query.filter_by(owner_id=username)
	if result is not None:
		data_id = list()
		title = list()
		summary = list()
		datafid = list()
		for records in result:
			data_id.append( records.id )
			title.append( records.name )
			summary.append( records.summary )
			datafid.append( records.savename )
		data = zip( data_id, title, summary, datafid )
	else:
		data = []
	return render_template('data/data_list.html', data=data, page='data')

# GENERAL FUNCTIONALITY IS TO UPDATE DATABASE AND THEN RE-RENDER
@app.route('/<username>/data/<type>')
@app.route('/<username>/data/<int:data_id>/<type>/')
def update_view(username=None, data_id=None, type=None ):
	if type == 'summary':
		page = 'data/data_summary.html'
	elif type == 'start_experiment':
		page = 'data/data_experiments.html'
		query = Data.query.filter_by(id=data_id).first()
		query.status = 'start'
		db.session.commit()
	elif type == 'experiments':
		page = 'data/data_experiments.html'
	elif type == 'visual':
		page = 'data/data_visual.html'
	elif type == 'settings':
		page = 'data/data_settings.html'
		
	if data_id is not None:
		datainfo = Data.query.filter_by(id=data_id).first()

	return render_template(page, data_id=data_id, datainfo=datainfo)

# DELETE DATASET
@app.route('/<username>/data/delete')
def delete_dataset(username=None, data_id=None):
	return render_template('data_modify.html')
	
# CHECK FILE INTEGRITY
ALLOWED_EXTENSIONS = set(['txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif'])
def allowed_file(filename):
	return '.' in filename and \
		   filename.rsplit('.', 1)[1] in ALLOWED_EXTENSIONS

# **** UPLOADS ***** 

# DOCUMENT CLOUD
@app.route('/<username>/upload/doc_cloud')
def upload_doc_cloud(username=None):
	return render_template('data/upload_doc_cloud.html')

# MANUAL UPLOADS
@app.route('/<username>/upload/manual')
def upload_manual(username=None):
	form = UploadForm()
	dog = User('beef','bulgogi','dog@example.com')
	#print User.query.all()
	obj = User.query.filter_by(username='beef').first()
	if obj is None:
		db.session.add(dog)
		db.session.commit()
	#print User.query.all()
	return render_template('data/upload_manual.html', status='processing', form=form)
		
@app.route("/stream_upload", methods=[ "GET","POST" ])
def stream_upload():
	mimetype = "text/event-stream"
	return Response(event_stream(channel='upload'), mimetype=mimetype)

@app.route('/<username>/upload/img', methods=['GET','POST'])
def upload_img(username=None):
	print request.method
	if request.method == 'POST':
		filename = request.headers.get('X_FILENAME')
		filename = secure_filename(filename)
		fid = request.files.getlist('file')[0] #grab only a single file
		first, ext = os.path.splitext(fid.filename)
		print fid.filename
		# use a fixed name for upload image for storage
		fid.filename = "upload_image" + ext
		print fid.filename
		# save this to some temp directory
		admin.save_to_tmp(g.user.username, fid)
		msgServer.publish('upload', "%s" % ('Image has been uploaded'))
		print fid
		return Response(status="200")

@app.route('/<username>/upload/drop', methods=['GET','POST'])
def upload_manual_start(username=None):
	form = UploadForm()
	msgServer.publish('upload', "%s" % ('Form validation occuring'))
	if request.method == 'POST':
		msgServer.publish('upload', "%s" % ('Form has been validated'))
		filename = request.headers.get('X_FILENAME')
		filename = secure_filename(filename)
		fid = request.files.getlist('file')[0] #grab only a single file
		
		# Extracts and saves the raw dataset to user directory
		msgServer.publish('upload', "%s" % ('Upload Finished...'))
		extract_dir, processed_dir, image_path, savefname = admin.save_data(g.user.username, fid, msgServer)
		
		# Preprocesses the dataset to preprare for analysis
		msgServer.publish('upload', "%s" % ('Preprocessing Data'))
		num_doc = 100 # number of document to process
		vocab_min_doc = 2 # minimum number of documents a word must be in
		max_percent = .5 # maximum percentage of documents a word can be in
		print processed_dir
		preprocess.tokenize_docs(extract_dir, processed_dir, num_doc, vocab_min_doc, max_percent, msgServer)
		
		# Write metadata to database
		# add_data_db(name, username, summary=None ):
		dataset_name = request.headers.get('X_TITLE')
		summary = request.headers.get('X_SUMMARY')
		# add the data to the database
		admin.add_data_db( name = dataset_name, username=g.user.username, savename=savefname, summary=summary)
		print Data.query.filter_by(owner_id=g.user.username).first()
		
		msgServer.publish('upload', "%s" % ('exit'))
		return Response(status="200")
	else:
		print "this didn't work"
		return Response(status="404")


@app.route('/upload_check')
def add_numbers():
	a = request.args.get('status', 0, type=int)
	b = request.args.get('b', 0, type=int)
	return jsonify(result=a + b)



# ***************  USER  ********************************* USER **************************
# The user_loader callback function used to reload the user object from the user id stored in the session
@lm.user_loader
def load_user(user_id):
	return User.query.get(int(user_id))
	
# The login page, authenticate users 
@app.route('/login', methods = ['GET', 'POST'])
def login():
	if g.user is not None and g.user.is_authenticated():
		return redirect(url_for('newsfeed'))
	form = LoginForm()
	if form.validate_on_submit():
		session['remember_me'] = form.remember_me.data
		if 'remember_me' in session:
			remember_me = session['remember_me']
			session.pop('remember_me', None)
		login_user(form.user, remember = remember_me)
		return redirect(url_for('newsfeed'))
	return render_template('login.html', form=form)
	
# Logout function requires login_required decorator which uses flask's login module
@app.route('/logout')
def logout():
	logout_user()
	flash('You have logged out')
	return render_template('login.html', form=LoginForm())

@app.route('/signup')
def signup():
	return render_template('signup.html')

@app.route('/profile')
@login_required
def profile():
	projects = ['daeil','anna','friends']
	datasets = range(1,50)
	return render_template('users/profile.html', projects = projects, datasets = datasets)

'''
# DATSET SUMMARY
@app.route('/<username>/data/summary')
@app.route('/<username>/data/<int:data_id>/summary/')
def data_view(username=None, data_id=None, type=None ):
	result = Data.query.filter_by(id=data_id).first()
	#title, summary, datafid = admin.dataset_info(username=username, data_id=data_id)
	return render_template('data/data_view.html', data_id=data_id, title=result.title, datafid=datafid, summary=summary)

# VISUALIZE D3.JS
@app.route('/<username>/data/visual')
@app.route('/<username>/data/<int:data_id>/visual/')
def data_visual(username=None, data_id=None):
	return render_template('data/data_visual.html',data_id=data_id)

	# VISUALIZE D3.JS
@app.route('/<username>/data/experiments')
@app.route('/<username>/data/<int:data_id>/experiments/')
def data_experiments(username=None, data_id=None):
	return render_template('data/data_experiments.html',data_id=data_id)

# EXPERIMENT SETTINGS
@app.route('/<username>/data/settings')
@app.route('/<username>/data/<int:data_id>/settings/')
def data_settings(username=None,data_id=None):
	return render_template('data/data_settings.html',data_id=data_id)
'''