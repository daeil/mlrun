#!/usr/bin/env python
# encoding: utf-8
import os
import sys
basedir = os.path.abspath(os.path.dirname(__file__))

# Must turn this off when in devlopment
DEBUG = True

# Flask WTF module requires these two settings
CSRF_ENABLED = True
SECRET_KEY = 'bulgogi'
# Path of our database file, required by flask-SQLAlchemy
SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(basedir, 'tmp/app.db')
#SQLALCHEMY_DATABASE_URI = 'postgresql://mlrun_admin@localhost/mlrun'
# Folder that stores our SQLAlchemy-migrate data files
SQLALCHEMY_MIGRATE_REPO = os.path.join(basedir, 'db_repository')
# Where to temporarily store the raw documents from a topic model upload
UPLOAD_FOLDER = 'mlrun/static/datasets/'
USER_DIRECTORY = 'mlrun/static/users/'
RANDOM_IMG_DIRECTORY = 'mlrun/static/images/random/'
sys.path.append('mlrun/bnpy')